Funciones para previsualizar en Emacs partes de un documento LaTeX
================================================================================

Contenido
-----------

-   [Nota previa e instalación](#nota-previa-e-instalación)
	-   [Instalación](#instalación)
		-   [Manual](#manual)
		-   [Automática](#automática)
-   [Para previsualizar imágenes](#para-previsualizar-imágenes)
	-   [Código para previsualizar
		imágenes](#código-para-previsualizar-imágenes)
-   [Para previsualizar tablas y otros
	entornos](#para-previsualizar-tablas-y-otros-entornos)
	-   [Código para previsualizar
		entornos](#código-para-previsualizar-entornos)

Nota previa e instalación
------------------------

Una serie de funciones para poder previsualizar partes de nuestro
documento LaTeX en Emacs, al estilo WYSIWYG. El procedimiento de
previsualización se realiza mediante *overlays* (capas superpuestas
temporales con atributos de texto y gráficos) y, por tanto, no es
destructivo. Para que funcione este código se requiere tener instalado
el paquete `smartparens`.

### Instalación

#### Manual

Basta con copiar los bloques de código de este documento ([Código para previsualizar
		imágenes](#código-para-previsualizar-imágenes)) y/o [Código para previsualizar
		entornos](#código-para-previsualizar-entornos)) y pegarlos en el archivo de inicio. Reinicar Emacs.

#### (Casi) automática

Descargar el fichero (`previsualiazar-latex.org`) y guardarlo en una
ruta accesible a Emacs. Abrir el fichero y, una vez dentro, generar el
archivo de código mediante `M-x org-babel-tangle <RET>` o con el atajo
`C-c C-v C-t`. Esto producirá el archivo `previsualizar-latex.el`. Sólo
quedaría cargarlo desde el *init* de Emacs añadiendo:

``` {.commonlisp org-language="emacs-lisp"}
(load "previsualizar-latex.el")
```

Para previsualizar imágenes
--------------------------

Están disponibles las siguientes funciones para llamar de forma
interactiva o mediante algún atajo creado a tal efecto:

`latex-imagen-figure`
:   Visualiza todas las imágenes contenidas en el entorno `figure` de
	nuestro documento LaTeX.

`latex-imagen-includegraphics`
:   Visualiza todas las imágenes que estén en un comando
	`\includegraphics{<archivo de
		 imagen>}`

`latex-recoge-imagen`
:   Detiene la previsualización y restituye la vista normal de código

### Código para previsualizar imágenes


``` {.commonlisp org-language="emacs-lisp"}
(defun copia-argumento ()
	 (interactive)
	 (sp-end-of-sexp)
	 (set-mark-command nil)
	 (sp-beginning-of-sexp)
	 (region-active-p)
	 (copy-region-as-kill (region-beginning) (region-end))
	 (deactivate-mark))

   (defun latex-imagen-figure ()
	 (interactive)
	 (let
   ((x (make-marker))
	(y (make-marker)))
   (save-excursion
   (goto-char (point-min))
   (while
	   (re-search-forward "\\\\begin{figure}" nil t)
	 (set-marker x (point))
	 (save-excursion
	   (re-search-forward "\\\\end{figure}" nil t)
	   (set-marker y (point)))
	 (re-search-forward (concat "\\\\" "includegraphics" "\\(\\[.+\\]\\)*" "{") nil t)
	 (copia-argumento)
	 (let
		 ((ov (make-overlay x y))
	  (imagen (create-image (expand-file-name (car kill-ring)) 'imagemagick nil :width 600)))
	   (image-refresh imagen)
	   (overlay-put ov 'overlay-imagen t)
	   (overlay-put ov 'display imagen))))))

   (defun latex-recoge-imagen ()
	 (interactive)
	 (remove-overlays nil nil 'overlay-imagen t))

   ;; lo mismo pero se acota sólo a `includegraphics'

   (defun latex-imagen-includegraphics ()
	 (interactive)
	 (let
   ((x (make-marker))
	(y (make-marker)))
   (save-excursion
   (goto-char (point-min))
   (while
	   (re-search-forward "\\\\includegraphics" nil t)
	 (beginning-of-line)
	 (set-marker x (point))
	 (save-excursion
	   (re-search-forward "}" nil t)
	   (set-marker y (point)))
	 (forward-line -1)
	 (re-search-forward (concat "\\\\" "includegraphics" "\\(\\[.+\\]\\)*" "{") nil t)
	 (copia-argumento)
	 (let
		 ((ov (make-overlay x y))
	  (imagen (create-image (expand-file-name (car kill-ring)) 'imagemagick nil :width 600)))
	   (image-refresh imagen)
	   (overlay-put ov 'overlay-imagen t)
	   (overlay-put ov 'display imagen))))))
```

![](./overlays-imagenes.gif)

Para previsualizar tablas y otros entornos
-----------------------------------------

Previsualiza el contenido de ciertos entornos, como tablas, ecuaciones,
etc. Hay que tener en cuenta lo siguiente:

1.  Para generar la imagen (en formato `png`) de previsualización, se
	ejecuta LaTeX en segundo plano, en un búfer temporal y con la clase
	`standalone`. Sólo se puede generar una previsualización por entorno
	(el cursor debe estar dentro del mismo entorno para que la
	previsualización se lleve a cabo).
2.  El documento LaTeX temporal lleva un preámbulo muy básico y
	genérico, por tanto la previsualización será aproximada. Para
	conseguir mayor fidelidad se pueden cargar más paquetes y/o
	definiciones personales de comandos y macros al código que sigue.
	Obsérvese la sintaxis de esa parte.

Están disponibles las siguientes funciones:

`latex-entorno`
:   Previsualiza el entorno donde tenemos situado el cursor.

`latex-oculta-entorno`
:   Detiene la previsualización y devuelve la vista normal de código.

### Código para previsualizar entornos


``` {.commonlisp org-language="emacs-lisp"}
(defun crea-imagen-entorno (imagen)
  (create-image (expand-file-name imagen) 'imagemagick nil :width 600))

(defun latex-entorno ()
  (interactive)
  (let
  ((x (make-marker))
   (y (make-marker)))
	(save-excursion
  (save-restriction
  (LaTeX-narrow-to-environment)
  (kill-ring-save (point-min) (point-max))))
	(save-window-excursion
  (shell-command "touch entorno-temp.tex")
  (write-region (concat "\\documentclass[convert={true},varwidth]{standalone}"
			  "\n\\pagestyle{empty}"
			  "\n\\usepackage{fontspec}"
			  "\n\\usepackage{xcolor}"
			  "\n\\usepackage{tabularx}"
			  "\n\\usepackage{amsmath}"
			  "\n\\setmainfont{Linux Libertine O}"
			  "\n\\begin{document}"
			  "\n\\pagecolor[RGB]{255,255,254}\n"
			  (car kill-ring)
			  "\n\\end{document}")
		  nil "entorno-temp.tex")
  (shell-command "lualatex -shell-escape entorno-temp.tex"))
	(re-search-backward (concat "\\\\" "begin"  "{.+}" "\\(\\[.+\\]\\)*") nil t)
	(set-marker x (point))
	(re-search-forward (concat "\\\\" "end"  "{.+}" "\\(\\[.+\\]\\)*") nil t)
	(set-marker y (point))
	(setq nuevoentorno (crea-imagen-entorno "entorno-temp.png"))
	(image-refresh nuevoentorno)
	(let
  ((ov (make-overlay x y)))
  (overlay-put ov 'overlay-entorno t)
  (overlay-put ov 'display nuevoentorno))))

(defun latex-oculta-entorno ()
  (interactive)
  (shell-command "rm entorno-temp*")
  (remove-overlays nil nil 'overlay-entorno t))
```

![](./overlays-tablas2.gif)
